import time

import numpy as np


# Usando NumPy
def suma_cuadrados_numpy(numbers):
    return np.sum(np.square(numbers))


# Sin NumPy (usando funciones built-in de Python)
def suma_cuadrados_builtin(numbers):
    return sum(x**2 for x in numbers)


if __name__ == "__main__":
    numeros = list(range(1, 1000001))  # Lista de números del 1 al 1,000,000

    # Medir el tiempo con NumPy
    start_time = time.time()
    resultado_numpy = suma_cuadrados_numpy(numeros)
    end_time = time.time()
    tiempo_numpy = end_time - start_time

    # Medir el tiempo sin NumPy
    start_time = time.time()
    resultado_builtin = suma_cuadrados_builtin(numeros)
    end_time = time.time()
    tiempo_builtin = end_time - start_time

    print(f"Resultado con NumPy: {resultado_numpy}")
    print(f"Tiempo con NumPy: {tiempo_numpy*1000: .2f} ms")

    print(f"Resultado sin NumPy: {resultado_builtin}")
    print(f"Tiempo sin NumPy: {tiempo_builtin*1000: .2f} ms")
