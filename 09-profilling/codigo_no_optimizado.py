# Uso de cProfile

import cProfile

# Función de ejemplo que realiza cálculos matemáticos
def calcular_fibonacci(n):
    if n <= 0:
        return 0
    elif n == 1:
        return 1
    else:
        return calcular_fibonacci(n - 1) + calcular_fibonacci(n - 2)


if __name__ == "__main__":
    n = 30  # Número para calcular Fibonacci

    # Profiling para medir el rendimiento de la función
    cProfile.run("resultado = calcular_fibonacci(n)")
