import cProfile

# Diccionario para almacenar resultados ya calculados
memoria_fibonacci = {}

def calcular_fibonacci_optimizado(n):
    # Verificar si ya hemos calculado Fibonacci para este valor
    if n in memoria_fibonacci:
        return memoria_fibonacci[n]

    # Si n es 0 o 1, retornar el valor directamente
    if n <= 0:
        return 0
    elif n == 1:
        return 1

    # Calcular Fibonacci y almacenar el resultado en memoria
    resultado = calcular_fibonacci_optimizado(n - 1) + calcular_fibonacci_optimizado(n - 2)
    memoria_fibonacci[n] = resultado
    return resultado

if __name__ == "__main__":
    n = 30  # Número para calcular Fibonacci

    # Profiling para medir el rendimiento de la función optimizada
    cProfile.run("resultado = calcular_fibonacci_optimizado(n)")