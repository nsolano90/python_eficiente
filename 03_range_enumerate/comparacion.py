# Diferencia entre range y enumerate

lista_sabores = ["vainilla", "chocolate", "pistacho", "mora"]

# Uso de range
print("Resultado con range")
for i in range(len(lista_sabores)):
    sabor = lista_sabores[i]
    print(f"{i + 1}: {sabor}")

# Uso de enumerate
print("Resultado con enumerate")
for i, sabor in enumerate(lista_sabores):
    print(f"{i + 1}: {sabor}")
