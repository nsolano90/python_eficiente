# Crear un diccionario de contactos
contactos = {"Juan": "juan@email.com", "María": "maria@email.com"}

# Búsqueda por clave
buscar_nombre = "Juan"
if buscar_nombre in contactos:
    print(f"Email de {buscar_nombre}: {contactos[buscar_nombre]}")

# Inserción
nuevo_contacto = {"Pedro": "pedro@email.com"}
contactos.update(nuevo_contacto)

# Eliminación por clave
eliminar_nombre = "María"
if eliminar_nombre in contactos:
    del contactos[eliminar_nombre]

print(contactos)
