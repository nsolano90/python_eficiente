# Crear un conjunto de colores
colores = {"rojo", "verde", "azul"}

# Búsqueda
buscar_color = "rojo"
if buscar_color in colores:
    print(f"{buscar_color} encontrado en el conjunto.")

# Inserción
nuevo_color = "amarillo"
colores.add(nuevo_color)

# Eliminación
eliminar_color = "verde"
if eliminar_color in colores:
    colores.remove(eliminar_color)

print(colores)
