# Crear una lista de números
numeros = [1, 2, 3, 4, 5]

# Búsqueda
buscar_numero = 3
if buscar_numero in numeros:
    print(f"{buscar_numero} encontrado en la lista.")

# Inserción
nuevo_numero = 6
numeros.append(nuevo_numero)  # Agregar al final de la lista

# Eliminación
eliminar_numero = 2
if eliminar_numero in numeros:
    numeros.remove(eliminar_numero)  # Eliminar el número 2

print(numeros)
