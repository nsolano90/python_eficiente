# Uso del operador *

edad_autos = [0, 9, 4, 8, 7, 20, 19, 1, 6, 15]
edad_autos_descendiente = sorted(edad_autos, reverse=True)

try:
    mas_antiguo, segundo_mas_antiguo = edad_autos_descendiente
except ValueError as ve:
    print(ve)

# Con slicing
mas_antiguo = edad_autos_descendiente[0]
segundo_mas_antiguo = edad_autos_descendiente[1]
otros = edad_autos_descendiente[2:]
print(f'Con slicing: {mas_antiguo} {segundo_mas_antiguo} {otros}')

# Con starred (operador *)
mas_antiguo, segundo_mas_antiguo, *otros = edad_autos_descendiente
print(f'Con unpacking: {mas_antiguo} {segundo_mas_antiguo} {otros}')
