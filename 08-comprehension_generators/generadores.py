# Uso de generadores
import sys


def even_numbers(n):
    i = 1
    while i < n:
        yield 2 * i
        i += 1


number = even_numbers(5)
print("Generador con next: ")
print(next(number))
print(next(number))
print(next(number))

number = even_numbers(10)
print("\nGenerador con lazo: ")
for n in number:
    print(n)

# Generadores vs Listas
lista = [n * 2 for n in range(1000)]
generador = (n * 2 for n in range(1000))
print("\nComparación en memoria: ")
print(f"Lista : {sys.getsizeof(lista)} bytes")
print(f"Generador : {sys.getsizeof(generador)} bytes")
