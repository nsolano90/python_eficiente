# Uso de comprensión de listas
import time

# Sin comprensión de listas
a = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

start = time.time()
squares = []
for x in a:
    squares.append(x**2)
print(f"Sin comprensión de listas: {squares}")
end = time.time()
print(f"Tiempo: {(end-start)*10e6:.2f} us")

# Con comprensión de listas
start = time.time()
squares = [x**2 for x in a]
print(f"\nCon comprensión de listas: {squares}")
end = time.time()
print(f"Tiempo: {(end-start)*10e6:.2f} us")

# Map vs Comprensión
even_squares = [x**2 for x in a if x % 2 == 0]
even_squares_map = map(lambda x: x**2, filter(lambda x: x % 2 == 0, a))
assert even_squares == list(even_squares_map)

# Generación de Diccionarios y Conjuntos
even_squares_dict = {x: x**2 for x in a if x % 2 == 0}
threes_cubed_set = {x**3 for x in a if x % 3 == 0}
print(f"\nDiccionario con comprensión: {even_squares_dict}")
print(f"Conjunto con comprensión: {threes_cubed_set}")
