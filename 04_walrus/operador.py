# uso del operador walrus

frutas = {
    "manzana": 10,
    "banana": 8,
    "limon": 5,
}


def no_stock():
    pass


def cortar_bananas(cont):
    return cont * 10


class NoBananas(Exception):
    pass


def hacer_batido(rodajas):
    return rodajas / 20


# Sin walrus
rodajas = 0
cont = frutas.get("banana", 0)
if cont >= 2:
    rodajas = cortar_bananas(cont)
try:
    batidos = hacer_batido(rodajas)
    print("Sin walrus")
    print(f"{batidos:.0f} batidos hechos")
except NoBananas:
    no_stock()

# Con walrus
rodajas = 0
if (cont := frutas.get("banana", 0)) >= 2:
    rodajas = cortar_bananas(cont)
    print("Con walrus")
    print(f"{batidos:.0f} batidos hechos")
try:
    batidos = hacer_batido(rodajas)
except NoBananas:
    no_stock()
