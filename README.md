# Uso eficiente de Python

La charla "Uso Eficiente de Python" se centra en destacar la importancia de optimizar el uso de recursos, tiempo y rendimiento al programar en Python. Se abordan temas como la optimización prematura y el profiling, la elección adecuada de estructuras de datos como listas, conjuntos y diccionarios, así como la eficiencia de comprensiones de listas y generadores para manipular datos de manera más efectiva. También se resalta la ventaja de utilizar bibliotecas externas para tareas especializadas. En última instancia, se enfatiza el equilibrio entre legibilidad y optimización, alentando a los asistentes a aplicar estos conceptos para lograr un uso eficiente y efectivo de Python en sus proyectos.


[Presentación de Google](https://docs.google.com/presentation/d/e/2PACX-1vRorGowLfkUVprrVU9V2V_7T1sSI2IMaOF_5pKxLvctogFNYfb9OhvCOr2ZUMHfN8B8UmEFjJFRhx-V/pub?start=true&loop=false&delayms=10000)

## Instalación

1. Clona este repositorio en tu máquina local.

    ```git clone https://gitlab.com/nsolano90/python_eficiente.git```

2. Accede al directorio del proyecto.

    ```cd python_eficiente```

3. Crea un entorno virtual (opcional, pero se recomienda).

    ```python3 -m venv .venv```

4. Activa el entorno virtual.

    ```source venv/bin/activate```

5. Ejecuta el comando ```pip install -r requirements.txt``` para instalar los requerimientos


## Uso
Dentro de cada carpeta se encuentran los códigos enseñados en la charla, debes ejecutarlos con tu IDE favorito o en la terminal de tu gusto.
