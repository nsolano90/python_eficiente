# Uso de sort para objetos


class Herramienta:
    def __init__(self, nombre, peso):
        self.nombre = nombre
        self.peso = peso

    def __repr__(self):
        return f"Herramienta({self.nombre!r}, {self.peso})"


herramientas = [
    Herramienta("martillo", 3.2),
    Herramienta("destornillador", 1.15),
    Herramienta("cincel", 0.65),
    Herramienta("broca", 0.15),
]

print("Desordenado:", repr(herramientas))
herramientas.sort(key=lambda x: x.nombre)
print("\nOrdenado: ", herramientas)
