# Diferencias entre los formatos de strings

KEY = "my_variable"
VALUE = 3.1416


c_tuple = "%-10s = %.2f" % (KEY, VALUE)
c_dict = "%(KEY)-10s = %(VALUE).2f" % {"KEY": KEY, "VALUE": VALUE}
str_args = "{:<10} = {:.2f}".format(KEY, VALUE)
str_kw = "{KEY:<10} = {VALUE:.2f}".format(KEY=KEY, VALUE=VALUE)
f_string = f"{KEY:<10} = {VALUE:.2f}"

assert c_tuple == c_dict == f_string
assert str_args == str_kw == f_string
